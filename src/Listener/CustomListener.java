package Listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class CustomListener implements ITestListener {
    public void onFinish(ITestContext arg0) {
        // TODO Auto-generated method stub
        System.out.println("Finish Test Execution..."+arg0.getName());
    }

    @Override
    public void onStart(ITestContext arg0) {
        // TODO Auto-generated method stub
        System.out.println("Start test execution..."+arg0.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
        // TODO Auto-generated method stub
        System.out.println("On test failed with success percentage..."+arg0.getName());
    }

    @Override
    public void onTestFailure(ITestResult arg0) {
        // TODO Auto-generated method stub
        System.out.println("On test failure..."+arg0.getName());
    }

    @Override
    public void onTestSkipped(ITestResult arg0) {
        // TODO Auto-generated method stub
        System.out.println("On test skipped..."+arg0.getName());
    }


    public void onTestStart(ITestResult arg0) {
        // TODO Auto-generated method stub
        System.out.println("On test start..."+arg0.getName());
    }

    @Override
    public void onTestSuccess(ITestResult arg0) {
        // TODO Auto-generated method stub
        System.out.println("On test success..."+arg0.getName());
    }
}
