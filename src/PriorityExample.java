import org.testng.annotations.Test;

public class PriorityExample {
    @Test(priority = 1)
    public void testOne(){
        System.out.println("This is test1");
    }
    @Test(priority = 2)
    public void testTwo(){
        System.out.println("This is test2");
    }
    @Test(priority = 3)
    public void testThree(){
        System.out.println("This is test3");
    }
    @Test(priority = 4,enabled = false)
    public void testFour(){
        System.out.println("This is test4");
    }
}
