import org.testng.annotations.Test;

public class FirstTestCase {
    @Test(priority = 1)
    public void setup(){
        System.out.println("Opening browser");
    }
    @Test(priority = 2)
    public void login(){
        System.out.println("Logging into the application");
    }
    @Test(priority = 3)
    public void tearDown(){
        System.out.println("Closing Browser");
    }
}
