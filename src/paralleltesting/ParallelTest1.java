package paralleltesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class ParallelTest1 {
    WebDriver driver;
    @Test
    public void logoTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","/Users/sreyanbiswas/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/");

        WebElement logo=driver.findElement(By.xpath("//*[@id=\"divLogo\"]/img"));
        Assert.assertTrue(logo.isDisplayed(),"logo not displayed");
        Thread.sleep(5000);
    }
    @Test
    public void homePage() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","/Users/sreyanbiswas/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/");

        String title = driver.getTitle();
        Assert.assertEquals("OrangeHRM",title,"Title is not displayed");
        Thread.sleep(5000);
    }
    @AfterTest
    void tearDown(){
        driver.quit();
    }
}
