import org.testng.Assert;
import org.testng.annotations.Test;

public class SecondTestCase {
    @Test(priority = 1)
    public void setup(){
        System.out.println("Opening browser");
    }
    @Test(priority = 2)
    public void addingCustomer(){
        System.out.println("adding customer to the application");
    }
    @Test(priority = 3)
    public void searchCustomer(){
        System.out.println("Searching customer");
        Assert.assertEquals(1,2);
    }
    @Test(priority = 4)
    public void tearDown(){
        System.out.println("Closing Browser");
    }
}
