import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterExample {
    WebDriver driver;
    @BeforeClass
    @Parameters({"url"})
    public void setup(String app){
        System.setProperty("webdriver.chrome.driver","/Users/sreyanbiswas/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.get(app);
    }
    @Test
    public void logoTest(){
        WebElement logo=driver.findElement(By.xpath("//*[@id=\"divLogo\"]/img"));
        Assert.assertTrue(logo.isDisplayed(),"logo not displayed");
    }
    @Test
    void homePage(){
        String title = driver.getTitle();
        Assert.assertEquals("OrangeHRM",title,"Title is not displayed");
    }
    @AfterClass
    void tearDown(){
        driver.quit();
    }
}
