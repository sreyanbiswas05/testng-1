import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProviderExample {

    @Test(dataProvider = "LoginData",dataProviderClass =CustomDataProvider.class)
    public void loginData(String email,String pwd){
        System.out.println(email+" "+pwd);
    }
}
