import org.testng.Assert;
import org.testng.annotations.Test;

public class DependencyExample {
    @Test
    void startCar(){
        System.out.println("This will start the car");
        Assert.fail();
    }
    @Test(dependsOnMethods = {"startCar"})
    void driveCar(){
        System.out.println("This will drive the car");
    }
    @Test(dependsOnMethods = {"driveCar"})
    void stopCar(){
        System.out.println("This will stop the car");
    }
    @Test(dependsOnMethods = {"stopCar","driveCar"},alwaysRun = true)
    void parkCar(){
        System.out.println("This will park the car");
    }
}
